
#include <soman.h>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"



TEST_CASE("Soman works on some input", "[soman]")
{
    REQUIRE(spread_soman(1) == 0);
    REQUIRE(spread_soman(2) == 0);
    REQUIRE(spread_soman(3) != 1);
}


TEST_CASE("Soman works on some other input", "[soman]")
{
    REQUIRE(spread_soman(1) == 0);
    REQUIRE(spread_soman(2) == 0);
    REQUIRE(spread_soman(3) != 1);
}

#ifdef DEBUG

TEST_CASE("A failing test only for debug mode", "[soman][debug]")
{
    REQUIRE(spread_soman(4) == 0);
}

#endif




#ifdef WITH_FAILING_TESTS

TEST_CASE("Soman fails on some input", "[soman][failing]")
{
    REQUIRE(spread_soman(4) == 1);
}

#endif



#ifdef WITH_CRASHING_TESTS

TEST_CASE("This test segfaults", "[soman][segfault]")
{
    int *m = new int[99999999999];
    int x[20];
    int val = x[99999999999999];
    REQUIRE(spread_soman(10) == val);
}

#endif