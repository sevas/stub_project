#pragma once

#define SARIN_COMPILER_MSVC     1
#define SARIN_COMPILER_GCC      2
#define SARIN_COMPILER_INTEL    3
#define SARIN_COMPILER_ARMCC    4
#define SARIN_COMPILER_ANDROID  5
#define SARIN_COMPILER_CLANG    6

#define SARIN_PLATFORM_WIN32        1
#define SARIN_PLATFORM_WIN64        2
#define SARIN_PLATFORM_LINUX_x86    3
#define SARIN_PLATFORM_LINUX_x64    4
#define SARIN_PLATFORM_LINUX_ARMv5  5
#define SARIN_PLATFORM_LINUX_ARMv7  6
#define SARIN_PLATFORM_ORBIS        7
#define SARIN_PLATFORM_DARWIN       8


#if defined( _MSC_VER )
    #define SARIN_COMPILER SARIN_COMPILER_MSVC
    #define SARIN_COMPILER_VERSION _MSC_VER
#elif defined(__arm__) && defined(__ARMCC_VERSION)
    #define SARIN_COMPILER SARIN_COMPILER_ARMCC
    /* the format is PVbbbb - P is the major version, V is the minor version,
     bbbb is the build number*/
    #define SARIN_COMPILER_VERSION (__ARMCC_VERSION)
#elif defined( __GNUC__ )
    // gcc family of compilers: gcc, android, clang
    #define SARIN_GEN_VERSION(major, minor, patch) (((major)*100) + \
        ((minor)*10) + \
        (patch))

    #define SARIN_COMPILER_VERSION SARIN_GEN_VERSION(__GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__)

    #if defined(__ANDROID__)
        #define SARIN_COMPILER SARIN_COMPILER_ANDROID
    #elif defined(__clang__)
        #define SARIN_COMPILER SARIN_COMPILER_CLANG
        #undef SARIN_COMPILER_VERSION
        #define SARIN_COMPILER_VERSION SARIN_GEN_VERSION(__clang_major__, __clang_minor__, __clang_patchlevel__)
    #else
        #define SARIN_COMPILER SARIN_COMPILER_GCC
    #endif
#else
    #error "Compilation error: Unsupported compiler."
#endif









#if defined( __WIN64__ ) || defined( _WIN64 )
    #define SARIN_PLATFORM SARIN_PLATFORM_WIN64
#elif defined( __WIN32__ ) || defined( _WIN32 )
    #define SARIN_PLATFORM SARIN_PLATFORM_WIN32
#elif defined(__linux__) || defined(__LINUX__)
    #define SARIN_PLATFORM_LINUX (1)

    #if (__TARGET_ARCH_ARM == 5 || __ARM_ARCH_5TEJ__ == 1 || __ARM_ARCH_5TE__ == 1 || __ARM_ARCH_5T__ == 1)
        #define SARIN_PLATFORM SARIN_PLATFORM_LINUX_ARMv5
        #define JVEC_NEON
    #elif (__TARGET_ARCH_ARM == 7 || __ARM_ARCH_7A__ == 1)
        #define SARIN_PLATFORM SARIN_PLATFORM_LINUX_ARMv7
    #elif defined(__x86) || defined(__x86__) || defined(__i386__) || defined(__i486__) || defined(__i586__) || defined(__i686__)
        #define SARIN_PLATFORM SARIN_PLATFORM_LINUX_x86
    #else
        #error "Compilation error: Unsupported version of Linux platform."
    #endif
#elif defined(__ORBIS__)
    #define SARIN_PLATFORM SARIN_PLATFORM_PS4
#elif defined(__APPLE__)
    #define SARIN_PLATFORM SARIN_PLATFORM_DARWIN
#else
    #error "Compilation error: Unsupported platform."
#endif

// Windows 32 or 64 bits defined.
#if(SARIN_PLATFORM == SARIN_PLATFORM_WIN32 || SARIN_PLATFORM == SARIN_PLATFORM_WIN64)
    #define SARIN_PLATFORM_WINDOWS
#endif








#if (SARIN_PLATFORM == SARIN_PLATFORM_WIN32 || SARIN_PLATFORM == SARIN_PLATFORM_WIN64) && !defined(__MINGW32__) && !defined(__CYGWIN__)
    #define SARIN_DECL  __stdcall
    #define SARIN_CDECL  __cdecl
#else
    #define SARIN_DECL
    #define SARIN_CDECL
#endif







#ifdef SARIN_STATIC_LIB
    #define SARIN_DLL_EXPORT
    #define SARIN_DLL_IMPORT
#else
    #if (SARIN_COMPILER == SARIN_COMPILER_MSVC)
        #define SARIN_DLL_EXPORT __declspec(dllexport)
        #define SARIN_DLL_IMPORT __declspec(dllimport)
    #elif (SARIN_COMPILER == SARIN_COMPILER_GCC || SARIN_COMPILER == SARIN_COMPILER_ARMCC)
        #if (SARIN_COMPILER_VERSION >= ((__GNUC__) * 100)) || (SARIN_COMPILER_VERSION >= (500000))
            #define SARIN_DLL_EXPORT         __attribute__ ((visibility ("default")))
            #define SARIN_DLL_IMPORT         __attribute__ ((visibility ("default")))
        #else
            #define SARIN_DLL_EXPORT
            #define SARIN_DLL_IMPORT
        #endif
    #else
        #define SARIN_DLL_EXPORT
        #define SARIN_DLL_IMPORT
    #endif
#endif

// import/export macros
#ifdef SARIN_EXPORTS
#define SARIN_API SARIN_DLL_EXPORT
#else
#define SARIN_API SARIN_DLL_IMPORT
#endif
